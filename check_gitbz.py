#!/usr/bin/python

import json
import logging
import os
import requests
import sys
from pprint import pprint


import git as gitpython

import gitbz.utils as gitbz

DISTGIT_GITBZ_QUERY_URL = "https://pkgs.devel.redhat.com/lookaside/gitbz-query.cgi"


def check_gitbz(repopath, namespace, project, oldrev, newrev, refname, _log):
    git_repo = gitpython.Repo(path=repopath)
    repo_commits = gitbz.get_commits(git_repo, oldrev, newrev)
    commits_data = []
    for repo_commit in repo_commits:
        speclines = gitbz.specAdditions(project, repo_commit)
        message = gitbz.get_commit_message(repo_commit)

        _log.debug("gitbz data for %s: %s", repo_commit.hexsha, message + speclines)

        single_commit = {
            "hexsha": repo_commit.hexsha,
            "files": gitbz.get_commit_files(repo_commit),
            "resolved": gitbz.bugzillaIDs("Resolves?:", message + speclines),
            "related": gitbz.bugzillaIDs("Related?:", message + speclines),
            "reverted": gitbz.bugzillaIDs("Reverts?:", message + speclines),
        }
        commits_data.append(single_commit)

    request_data = {
        "package": project,
        "namespace": namespace,
        "ref": refname,
        "commits": commits_data,
    }

    _log.info("Sending data to gitbz API to verify hooks result ...")
    _log.debug("Data sent: %s", json.dumps(request_data, sort_keys=True, indent=2))

    res = requests.post(DISTGIT_GITBZ_QUERY_URL, json=request_data, timeout=905)

    _log.debug("Response from gitbz API: %s", res.text)
    if not res.ok:
        _log.warning("Failed gitbz check for request: %s", request_data)

    return json.loads(res.text)


def log_setup():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Log all messages into the debug log
    debug_handler = logging.FileHandler('{}/debug.log'.format(os.environ["CI_PROJECT_DIR"]))
    debug_handler.setLevel(logging.DEBUG)
    logger.addHandler(debug_handler)

    # Also log everything INFO and higher to the console
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    logger.addHandler(console_handler)

    return logger


def main():
    logger = log_setup()

    logger.debug("Working Directory: {}".format(os.getcwd()))

    namespace = os.environ["CI_PROJECT_NAMESPACE"].rsplit("/")[-1]

    result = check_gitbz(
        repopath=os.environ["CI_PROJECT_DIR"],
        namespace=namespace,
        project=os.environ["CI_PROJECT_NAME"],
        oldrev=os.environ["CI_MERGE_REQUEST_DIFF_BASE_SHA"],
        newrev=os.environ["CI_COMMIT_SHA"],
        refname="refs/heads/{}".format(
            os.environ["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"]
        ),
        _log=logger,
    )

    logger.info(result["logs"])
    if result["result"] != "ok":
        logger.warning(result["error"])
        sys.exit(1)


if __name__ == "__main__":
    main()
