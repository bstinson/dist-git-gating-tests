FROM quay.io/centos/centos:7
ARG internal_cert_rpm
ARG internal_rcm_repo
RUN yum -y --nogpgcheck install "${internal_cert_rpm}" && \
    curl -L -o /etc/yum.repos.d/rcm.repo "${internal_rcm_repo}" && \
    yum -y install python-dist-git-utils python-requests GitPython && \
    touch /usr/lib/python2.7/site-packages/gitbz/__init__.py

COPY check_gitbz.py /usr/bin/check_gitbz
